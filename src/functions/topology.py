import random
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors


class Topology:
    """
    Class that holds all information about given Schelling game
    """

    def __init__(self, empty_nodes, n_b, n_r, graph=nx.Graph(), k=2):
        """
        Constructor
        :param empty_nodes: number of empty nodes (int)
        :param n_b: number of strategic blue agents (int)
        :param n_r: number of strategic red agents (int)
        :param graph: networkx graph to construct assignment on (networkx.Graph)
        :param k: number of colors (does not work or k != 2) (int)
        """
        self.graph = graph
        self.empty_nodes = empty_nodes
        self.n = n_b + n_r
        self.n_b = n_b
        self.n_r = n_r
        self.k = k

    def show(self):
        """
        Prints info about topology in terminal and shows plot of the topology
        :return: void
        """
        print('#empty nodes: ' + str(self.empty_nodes) + ', #agents: ' + str(self.n) + ', #colors: '
              + str(self.k))
        node_colors = [mcolors.CSS4_COLORS[node[1]['color']] for node in self.graph.nodes(data=True)]

        # draw the graph with labels and node colors
        nx.draw(self.graph, with_labels=True, node_color=node_colors)
        plt.show()

    def _create_assignment(self):
        """
        Calculates list of colors for given topology
        :return:
        List of colors
        """
        attr = []
        tmp = ['red'] * self.n_r
        attr.extend(tmp)
        tmp = ['blue'] * self.n_b
        attr.extend(tmp)
        tmp = ['white'] * self.empty_nodes
        attr.extend(tmp)
        random.shuffle(attr)

        attrs = dict()
        for i in range(self.empty_nodes + self.n):
            attrs[i] = {'color': attr[i], 'stubborn': False}
        return attrs

    def agents(self):
        """
        returns ids of all agents in a graph
        :return: list of ints (list)
        """
        agents = []
        for node_id, values in self.get_nodes():
            if values['color'] != 'white':
                agents.append(node_id)
        return agents

    def is_stubborn(self, agent):
        """
        Checks whether agent is stubbonr
        :param agent: agent id (int)
        :return: boolean
        """
        return self.graph.nodes[agent]['stubborn']

    def empty(self):
        """
        returns ids of all empty nodes in a graph
        :return: list of ints (list)
        """
        empty = []
        for node_id, values in self.get_nodes():
            if values['color'] == 'white':
                empty.append(node_id)
        return empty

    def get_nodes(self):
        """
        Gets all nodes from the graph
        :return: ůist of tuples (node_id, node_values) (list)
        """
        return self.graph.nodes(data=True)

    def get_neighbors(self, agent):
        """
        Calculates id's of neighbors of agent
        :param agent: node id (int)
        :return: returns number of red, blue agents and empty nodes (tuple)
        """
        neighbors = self.graph.neighbors(agent)
        red_neighbors = 0
        blue_neighbors = 0
        for neighbor in neighbors:
            if self.graph.nodes[neighbor]['color'] == 'blue':
                blue_neighbors += 1
            elif self.graph.nodes[neighbor]['color'] == 'red':
                red_neighbors += 1
        return red_neighbors, blue_neighbors

    def get_degree(self, agent):
        """
        calculates degree of given node
        :param agent: node id (int)
        :return: degree of given node (int)
        """
        return self.graph.degree(agent)

    def utility(self, agent):
        """
        calculates utility of agent
        :param agent: node_id (int)
        :return:
        """
        red_neighbors, blue_neighbors = self.get_neighbors(agent)
        # calculate utility of agent
        if (blue_neighbors + red_neighbors) == 0:
            return 0
        if self.graph.nodes[agent]['color'] == 'blue':
            return blue_neighbors / (blue_neighbors + red_neighbors)
        return red_neighbors / (blue_neighbors + red_neighbors)

    def create_random_graph(self):
        """
        Creates random graph from topology values
        :return: void
        """
        self.graph = nx.fast_gnp_random_graph(self.n + self.empty_nodes, 0.5)
        attr = self._create_assignment()
        nx.set_node_attributes(self.graph, attr)

    def create_random_tree(self):
        """
        Creates random tree graph from topology values
        :return: void
        """
        self.graph = nx.generators.trees.random_tree(self.n + self.empty_nodes)
        attr = self._create_assignment()
        nx.set_node_attributes(self.graph, attr)

    def create_stubborn(self, n):
        """
        Makes random n agents stubborn
        :param n: number of stubborn agents (int)
        :return: void
        """
        while n > 0:
            pos = random.randrange(0, self.n)
            if self.make_stubborn(pos):
                n -= 1

    def make_stubborn(self, node_id):
        """
        Creates agent at given id stubborn
        :param node_id: id of a node in a graph (int)
        :return: agent was made stubborn (boolean)
        """
        self.graph.nodes[node_id]['stubborn'] = True
        made = False
        color = self.graph.nodes[node_id]['color']
        if color == 'blue' and self.n_b >= 1:
            self.n_b -= 1
            made = True
        elif color == 'red' and self.n_r >= 1:
            self.n_r -= 1
            made = True
        return made

    def set_agents(self, assignment):
        """
        Places given assignment on our graph
        :param assignment: list of colors (list)
        :return: void
        """
        pos = 0
        for node_id, values in self.get_nodes():
            if not values['stubborn']:
                values['color'] = assignment[pos]
                pos += 1

    def swap(self, n1, n2):
        """
        swaps two node values
        :param n1: id of first node (int)
        :param n2: id of second node (int)
        :return: void
        """
        self.graph.nodes[n1]['color'], self.graph.nodes[n2]['color'] = self.graph.nodes[n2]['color'], \
            self.graph.nodes[n1]['color']

    def sw(self):
        """
        calculates social welfare of the topology
        :return: social welfare (int)
        """
        sw = 0
        for node_id, values in self.get_nodes():
            if values['color'] == 'white':
                continue
            sw += self.utility(node_id)
        return sw
