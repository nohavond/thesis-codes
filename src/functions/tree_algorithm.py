import math
from collections import deque
from topology import Topology

from treelib import Tree


class _Table:
    """
    Class used for tabulation of parameters for each node
    and value of the table
    """

    def __init__(self, c='', n=(0, 0, 0), k=(0, 0), u_l=[0, 0, 0, 0], u_t=[0, 0, 0]):
        self.c = c  # color
        self.n = n  # number of agents (blue, red, empty)
        self.k = k  # number of children (blue, red)
        self.u_l = u_l  # lower bound value for utility (blue, red)
        self.u_t = u_t  # upper bound value for utility (blue, red)

        # additional values for each table
        self.table_value = 0

    def set_value(self, value):
        self.table_value = value

    def combine_tables(self, table_a, table_b):
        pass


class Tables:
    """
    Class that holds all tables for each color and identifies stubborn agents
    """

    def __init__(self):
        self.tables = {}
        # info if agent is stubborn
        self.is_stubborn = False

    def set_stubborn(self, stubborn):
        self.is_stubborn = stubborn


def _child(top, node_id):
    """
    :param top: topology (treelib.Tree)
    :param node_id: id of node
    :return: children of node_id (treelib.Node list)
    """
    return top.children(node_id)


def _tree(top, node_id):
    """
    Returns subtree starting from given node id
    :param top:
    :param node_id:
    :return:
    """
    return top.subtree(node_id)


def _fill_leaf_table(node, color):
    """
    Fills leaf nodes based on Equation 4 from Theorem 3.4 from article
    https://doi.org/10.1016/j.artint.2021.103576
    :param node: node (treelib.Node)
    :return: void
    """
    if node.data.is_stubborn:
        return

    if color == 'blue':
        n = (1, 0, 0)
    else:
        n = (0, 1, 0)
    # create new table for
    table = _Table(c=color, n=n)
    table.set_value(0)
    # set n for leaf nodes
    if color != 'empty':
        node.data.tables[color] = table
    if color == 'empty':
        table.n = (0, 0, 1)
        node.data.tables[color] = table


def _blue_color_evaluator(color):
    """
    Evaluator that checks weather input color is blue
    :param color: String
    :return: 1 or 0 (integer)
    """
    if color == 'blue':
        return 1
    return 0


def _red_color_evaluator(color):
    """
    Evaluator that checks weather input color is red
    :param color: String
    :return: 1 or 0 (integer)
    """
    if color == 'red':
        return 1
    return 0


def _empty_color_evaluator(color):
    """
    Evaluator that checks weather input color is empty
    :param color: String
    :return: 1 or 0 (integer)
    """
    if color == 'empty':
        return 1
    return 0


def _check_conditions_theta(stubborn, tau, theta, theta_next, red, blue, empty):
    """
    Check if input tables suffices conditions and no agents wants to deviate
    :param stubborn: stubbornness (Boolean)
    :param tau: child table (_Table class)
    :param theta: table l-1 (_Table class)
    :param theta_next: table l (_Table class)
    :param red: number of red agents (int)
    :param blue: number of blue agents (int)
    :param empty: number of empty nodes (int)
    :return: Boolean
    """
    if theta_next.n[0] > blue or theta_next.n[1] > red or theta_next.n[2] > empty:
        return False
    # we do not care about deviations of stubborn agent
    if stubborn:
        return True

    # check deviation conditions
    tau_u_b_low, tau_u_r_low, tau_u_b_cross, tau_u_r_cross = tau.u_l
    tau_u_b_max, tau_u_r_max, tau_top = tau.u_t
    tau_k_b, tau_k_r = tau.k
    tau_c = tau.c

    theta_u_b_low, theta_u_r_low, theta_u_b_cross, theta_u_r_cross = theta.u_l
    theta_u_b_max, theta_u_r_max, theta_top = theta.u_t
    theta_k_b, theta_k_r = theta.k
    theta_c = theta.c

    theta_next_u_b_low, theta_next_u_r_low, theta_next_u_b_cross, theta_next_u_r_cross = theta_next.u_l
    theta_next_u_b_max, theta_next_u_r_max, theta_next_top = theta_next.u_t
    theta_next_k_b, theta_next_k_r = theta_next.k
    theta_next_c = theta_next.c
    try:
        if tau_c == 'blue':
            if (tau_k_b + _blue_color_evaluator(theta_c) /
                (tau_k_r + tau_k_b + (1 - _empty_color_evaluator(theta_c)))) < theta_next_u_b_low:
                return False
        elif tau_c == 'red':
            if (tau_k_r + _red_color_evaluator(theta_c) /
                (tau_k_r + tau_k_b + (1 - _empty_color_evaluator(theta_c)))) < theta_next_u_r_low:
                return False
    # ignore division by zero
    except ZeroDivisionError:
        pass

    if theta_u_b_low < theta_next_u_b_low:
        return False
    if theta_u_r_low < theta_next_u_r_low:
        return False

    if theta_c == 'blue':
        if theta_top > theta_next_top:
            return False
        if tau_c == 'empty':
            try:
                if tau_k_b / (tau_k_b + tau_k_r) > theta_next_top:
                    return False
            except ZeroDivisionError:
                pass

    if theta_c == 'red':
        if theta_top > theta_next_top:
            return False
        if tau_c == 'empty':
            try:
                if tau_k_r / (tau_k_b + tau_k_r) > theta_next_top:
                    return False
            except ZeroDivisionError:
                pass

    if tau_u_b_cross < theta_next_u_b_cross:
        return False
    if tau_u_r_cross < theta_next_u_b_cross:
        return False

    return True


def _calculate_children_utility(tau, color, tau_color, is_stubborn):
    """
    Calculates utility of children based on input value of parent
    :param tau: _Table() class
    :param color: parent color (String)
    :param tau_color: child color (String)
    :return:
    """

    # remove to not consider stubborn
    # if is_stubborn:
    # return 0
    k_b = tau.k[0]
    k_r = tau.k[1]
    if tau_color == 'blue':
        try:
            result = (k_b + _blue_color_evaluator(color)) / (k_b + k_r + _red_color_evaluator(color) +
                                                             _blue_color_evaluator(color))
        except ZeroDivisionError:
            return 0
        return result
    elif tau_color == 'red':
        try:
            result = (k_r + _red_color_evaluator(color)) / (k_b + k_r + _red_color_evaluator(color) +
                                                            _blue_color_evaluator(color))
        except ZeroDivisionError:
            return 0
        return result
    return 0


def _fill_utilities(tau, theta, next_theta):
    """
    Function fills utilities for given agents tables
    :param tau: tau table
    :param theta: theta table
    :param next_theta: next theta table
    :return: void
    """
    children_utility = _calculate_children_utility(tau, theta.c, tau.c, False)
    if next_theta.c == 'red':
        next_theta.u_l[3] = min(tau.u_l[3], children_utility)
        next_theta.u_l[2] = min(tau.u_l[1], children_utility)
        next_theta.u_t[1] = max(tau.u_t[1], children_utility)
    elif next_theta.c == 'blue':
        next_theta.u_l[2] = min(tau.u_l[2], children_utility)
        next_theta.u_l[0] = min(tau.u_l[0], children_utility)
        next_theta.u_t[0] = max(tau.u_t[0], children_utility)

    # get top max
    top = 0
    try:
        if tau.c == 'blue':
            top = (tau.k[0] + _blue_color_evaluator(next_theta.c)) / (tau.k[0] + tau.k[1] + _red_color_evaluator(next_theta.c) + _blue_color_evaluator(next_theta.c))
        elif tau.c == 'red':
            top = (tau.k[1] + _red_color_evaluator(next_theta.c)) / (tau.k[0] + tau.k[1] + _red_color_evaluator(next_theta.c) + _blue_color_evaluator(next_theta.c))
    except ZeroDivisionError:
        top = 0
    next_theta.u_t[2] = max(top, tau.u_t[2])

def _calculate_root_utility(tables):
    """
    Calculates final utility of a root node
    :param tables: all tables in the root node for each color (dict(_Table class))
    :return:
    """
    for color, theta in tables.items():
        if color == 'blue':
            theta.table_value += (1 + theta.k[0]) / (1 + theta.k[0] + theta.k[1])
        elif color == 'red':
            theta.table_value += (1 + theta.k[1]) / (1 + theta.k[0] + theta.k[1])


def _remove_thetas(thetas, rng):
    """
    Removes tables from previous iteration
    :param thetas: list of tables (list(_Table class)
    :param rng: range (int)
    :return: void
    """
    for i in range(rng):
        thetas.pop(0)


def _select_best_theta(thetas):
    """
    Chooses best possible table from array
    :param thetas: array of tables (_Table class)
    :return: _Table() class
    """
    best_theta = _Table()
    best_theta.set_value(-math.inf)
    for theta in thetas:
        if theta.table_value > best_theta.table_value:
            best_theta = theta
    return best_theta


def _create_intermediate_tables(top, node, color, red, blue, empty):
    """
    Creates intermediate tables for each child node and combines them into
    new table for parent node
    :param top:
    :param node:
    :param color:
    :param red:
    :param blue:
    :param empty:
    :return:
    """
    children = _child(top, node.identifier)
    # fill theta zero
    if color == 'blue':
        theta_0 = _Table(n=(1, 0, 0))
    elif color == 'red':
        theta_0 = _Table(n=(0, 1, 0))
    else:
        theta_0 = _Table(n=(0, 0, 1))

    array_len = 0
    thetas = [theta_0]
    for child in children:
        _remove_thetas(thetas, array_len)
        array_len = len(thetas)
        for idx in range(array_len):
            theta = thetas[idx]
            for tau_color in ('red', 'blue', 'empty'):
                try:
                    tau = child.data.tables[tau_color]
                except KeyError:
                    continue
                # calculate new theta k and n values
                theta_n = (tau.n[0] + theta.n[0], tau.n[1] + theta.n[1], tau.n[2] + theta.n[2])
                theta_k = (tau.k[0] + _blue_color_evaluator(tau_color), tau.k[1] + _red_color_evaluator(tau_color))
                # create new table for theta
                theta_next = _Table(c=color, n=theta_n, k=theta_k)
                theta_next.set_value(theta.table_value + tau.table_value +
                                     _calculate_children_utility(tau, color, tau_color, child.data.is_stubborn))
                # calculate utilities for given agent table
                _fill_utilities(tau, theta, theta_next)
                # check additional conditions regarding utilities
                # set to -inf if not met
                if not _check_conditions_theta(node.data.is_stubborn, tau, theta, theta_next, red, blue, empty):
                    theta_next.set_value(-math.inf)
                thetas.append(theta_next)

    return _select_best_theta(thetas)


def _fill_parent_table(top, node, color, red, blue, empty):
    """
    Function fills agent table for given color
    :param top: topology (treelib.Tree)
    :param node: given agent node (treelib.Node)
    :param color: given agent color (String)
    :return: void
    """
    if node.data.is_stubborn:
        try:
            node.data.tables[color] = _create_intermediate_tables(top, node, color, red, blue, empty)
        except KeyError:
            return
    # fill table with thetas for given color
    theta_l = _create_intermediate_tables(top, node, color, red, blue, empty)
    node.data.tables[color] = theta_l


def _get_leaves(top):
    """
    :param top: tppology (Topology class)
    :return: list of leaf nodes
    """
    return top.leaves()


def _traverse_order(top):
    """
    Calculates traverse order for algorithm using BFS
    :param top: topology (treelib.Tree)
    :return: traverse order (list)
    """
    root = top.get_node(top.root)
    queue = deque()
    levels = []
    queue.append((root, 0))

    while queue:
        node, depth = queue.popleft()
        if depth >= len(levels):
            levels.append([])
        levels[depth].append(node)

        for child in _child(top, node.identifier):
            queue.append((child, depth + 1))
    # return order from last depth
    return levels[::-1]


def _calculate_mswe(empty, red, blue, top):
    """
    Calculates MSWE
    :param blue: number of blue agents (int)
    :param red: number of red agents (int)
    :param empty: number of empty nodes (int)
    :param top: topolgoy (Topology class)
    :return: MSWE (float)
    """
    # solve trivial case
    if red == 0:
        return blue if blue > 1 else 0
    elif blue == 0:
        return red if red > 1 else 0
    # get traverse order from bottom of a tree
    traverse_order = _traverse_order(top)
    for nodes in traverse_order:
        for node in nodes:
            # for each agent we try every color and her table
            for color in ('red', 'blue', 'empty'):
                if node.is_leaf():
                    _fill_leaf_table(node, color)
                else:
                    _fill_parent_table(top, node, color, red, blue, empty)
    node = top.get_node(top.root)
    tables = node.data.tables
    # finish by adding root's utility to tables
    _calculate_root_utility(tables)
    mswe = 0
    for color, theta in tables.items():
        if theta.table_value > mswe:
            mswe = theta.table_value
    return mswe


def tree_algorithm(top):
    empty = top.empty_nodes
    blue = top.n_b
    red = top.n_r
    tree, blue_stubborn, red_stubborn = _networkx_to_treelib(top)
    return _calculate_mswe(empty, red + red_stubborn, blue + blue_stubborn, tree)


def _traverse(tree, graph, node, visited, red_stubborn, blue_stubborn, parent=None):
    """
    Traverses networkx graph and converts it to faster treelib library
    :param tree: treelib.Tree
    :param graph: networkx.Graph
    :param node: root node id (int)
    :param visited: set of visited nodes (set())
    :param parent: parent of current node (int)
    :return: converted tree (treelib.Tree)
    """
    visited.add(node)
    node_data = graph.nodes[node]
    tree.create_node(identifier=node, parent=parent, data=Tables())
    # make treenode stubborn
    if node_data['stubborn']:
        if node_data['color'] == 'red':
            red_stubborn += 1
        elif node_data['color'] == 'blue':
            blue_stubborn += 1
        tree_node = tree.get_node(node)
        tree_node.data.set_stubborn(True)
        color = node_data['color']
        tree_node.data.tables[color] = _Table(c=color, n=(_blue_color_evaluator(color), _red_color_evaluator(color), 0))
    # traverse nodes
    children = graph.neighbors(node)
    for child in children:
        # traverse only nodes that were not processed
        if child not in visited:
            _traverse(tree, graph, child, visited, red_stubborn, blue_stubborn, parent=node)


def _networkx_to_treelib(t):
    """
    Converts topology to treelib object for faster processing
    :param t: topology (Topology class)
    :return: converted tree (treelib.Tree)
    """
    visited = set()
    tree = Tree()
    blue_stubborn, red_stubborn = 0, 0
    _traverse(tree, t.graph, 0, visited, red_stubborn, blue_stubborn)
    return tree, blue_stubborn, red_stubborn

