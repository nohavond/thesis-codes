import torch
from torch_geometric.data import Data


def _create_tree1():
    """
    Generates tree topology in MSWE with 4 nodes and one stubborn agent
    """
    edge_index = torch.tensor([[0, 1],
                               [1, 0],
                               [1, 2],
                               [2, 1],
                               [2, 3],
                               [3, 2]], dtype=torch.long)
    # features of each node -> [utility, # red neighbors, # blue neighbors, color, degree, stubborn]
    x = torch.tensor([[1, 0, 1, 1, 1, 0], [1, 0, 1, 1, 2, 1], [0, 0, 0, 0, 2, 0], [0, 0, 0, -1, 1, 1]], dtype=torch.float)
    return Data(x=x, edge_index=edge_index.t().contiguous())


def _create_tree2():
    """
    Generates tree topology that is not MSWE with 4 nodes and one stubborn agent
    """
    edge_index = torch.tensor([[0, 1],
                               [1, 0],
                               [1, 2],
                               [2, 1],
                               [2, 3],
                               [3, 2]], dtype=torch.long)
    # features of each node -> [utility, # red neighbors, # blue neighbors, color, degree, stubborn]
    x = torch.tensor([[0, 0, 0, 1, 1, 1], [0, 0, 2, -1, 2, 0], [0, 1, 0, 1, 2, 0], [0, 0, 1, 0, 1, 0]],
                     dtype=torch.float)
    return Data(x=x, edge_index=edge_index.t().contiguous())


def _create_tree3():
    """
    Generates tree topology with 2 agents, both of them being stubborn stubborn agent
    """
    edge_index = torch.tensor([[0, 1],
    			       [0, 2]], dtype=torch.long)
    
    # features of each node -> [utility, # red neighbors, # blue neighbors, color, degree, stubborn]
    x = torch.tensor([[0, 2, 0, 1, 2, 0], [0, 0, 1, -1, 1, 0], [0, 0, 1, -1, 1, 0]],
                     dtype=torch.float)
    return Data(x=x, edge_index=edge_index.t().contiguous())

def _create_general2():
    """
    Generates cycle that is not MSWE with 4 nodes and one stubborn agent
    """
    edge_index = torch.tensor([[0, 1],
                               [1, 0],
                               [1, 2],
                               [2, 1],
                               [2, 3],
                               [3, 2],
                               [3, 0],
                               [0, 3]], dtype=torch.long)
    # features of each node -> [utility, # red neighbors, # blue neighbors, color, degree, stubborn]
    x = torch.tensor([[0, 2, 1, 1, 2, 1], [0, 0, 2, 0, 2, 0], [0, 0, 0, 1, 2, 0], [0, 0, 2, 0, 2, 0]],
                     dtype=torch.float)
    return Data(x=x, edge_index=edge_index.t().contiguous())


def _create_general3():
    """
    Generates a comple graph with 4 nodes, with two stubborn agents from each color and 2 strategic agents
    """
    edge_index = torch.tensor([[0, 1],
                               [1, 0],
                               [1, 2],
                               [2, 1],
                               [2, 3],
                               [3, 2],
                               [3, 0],
                               [0, 3],
                               [0, 2],
                               [2, 0],
                               [1, 3],
                               [3, 1]], dtype=torch.long)
    # features of each node -> [utility, # red neighbors, # blue neighbors, color, degree, stubborn]
    x = torch.tensor([[0.5, 2, 1, 1, 3, 1], [0.5, 1, 2, -1, 3, 1], [0.5, 2, 1, 1, 3, 0], [0.5, 1, 2, -1, 3, 0]],
                     dtype=torch.float)
    return Data(x=x, edge_index=edge_index.t().contiguous())


def generate_trees():
    """
    Returns three generated tree assignments
    """
    return _create_tree1(), _create_tree2(), _create_tree3()



def generate_generals():
    """
    Returns three generated general assignments
    """
    return  _create_tree2(), _create_general2(), _create_general3()
