from more_itertools import distinct_permutations
from topology import Topology
import copy
import math

def _colors_permutations(colors):
    return list(distinct_permutations(colors))


# create list of possible colors to permutation
def _color_list(empty, blue, red):
    colors = []
    for i in range(empty):
        colors.append('white')
    for i in range(blue):
        colors.append('blue')
    for i in range(red):
        colors.append('red')
    return _colors_permutations(colors)


def calculate_equilibrium(t):
    """
    Calculates equilibriums for given topology
    :param t: topology (class Topology)
    :return: list of topologies in equilibrium (list)
    """
    equilibriums = []
    permutations = _color_list(t.empty_nodes, t.n_b, t.n_r)
    for assignment in permutations:
        equilibrium = True
        t.set_agents(assignment)
        for agent in t.agents():
            if t.is_stubborn(agent):
                continue
            for empty in t.empty():
                u1 = t.utility(agent)
                t.swap(agent, empty)
                u2 = t.utility(empty)
                t.swap(agent, empty)  # swapping nodes in G back
                if u1 < u2:
                    equilibrium = False
                    break
            # if not equilibrium do not continue
            if not equilibrium:
                break
        if equilibrium:
            equilibriums.append(copy.deepcopy(t))
    return equilibriums
    
    

def calculate_mswe(t):
    """
    Calculates mswe assignment for given topology
    :param t: topology (class Topology)
    :return: mswe assignment (Topology class)
    """
    equilibriums = calculate_equilibrium(t)
    mswe_idx = -1
    sw = -math.inf
    for idx in range(len(equilibriums)):
        curr_sw = equilibriums[idx].sw()
        if curr_sw > sw:
            mswe_idx = idx
            sw = curr_sw

    return equilibriums[mswe_idx]

    
    
    
    
    
    
    
    
    
    
    

	
