#!/usr/bin/env bash
         pip uninstall numpy==1.23.4;
         pip uninstall torch==1.13.0;
         pip uninstall networkx== 3.0;
         pip uninstall torch_geometric==2.3.0;
         pip uninstall matplotlib==3.6.2;
         pip uninstall more-itertools==8.10.0;
         pip uninstall treelib==1.6.1;
 
 echo "Your computer is clean!";


