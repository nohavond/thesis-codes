# Thesis - codes

In this repository there are all codes regarding thesis Schelling games on graphs. Feel free to browse Jupyter notebooks and source codes.

## Contents


Description of .ipynb files:

	gnn_testing: Notebook where you can test our model on simple topologies or on your own topology.

	gnn_training: Notebook showing how we trained our neural network.

	tree_algorithm: Notebook explaing our polynomial algorithm on trees. Furthermore, we provide detailed analysis for results in our thesis.
    
