from topology import Topology
import torch
from torch_geometric.data import Data


def _get_node_features(G):
    """
    Creates features for all nodes in a graph
    :param G: graph - networkx.Graph
    :return: array of features
    """
    topology = Topology(0, 0, 0, graph=G)
    node_features = []
    for node_id, values in topology.get_nodes():
        utility = topology.utility(node_id)
        red_neighbors, blue_neighbors = topology.get_neighbors(node_id)
        degree = topology.get_degree(node_id)
        color = 0
        if values['color'] == "blue":
            color = 1
        elif values['color'] == "red":
            color = -1
        stubborn = 0
        if values['stubborn']:
            stubborn = 1
        node_features.append([utility, red_neighbors, blue_neighbors, color, degree, stubborn])
    return torch.tensor(node_features, dtype=torch.float)


def to_data(G):
    """
    Converts networkx.Graph() to data that can be used on GNN
    :param G: networkx.Graph()
    :return: data - torch_geometric.data.Data
    """
    edge_index = []
    for i, j in G.edges:
        edge_index.append([i, j])
        edge_index.append([j, i])

    edge_index = torch.tensor(edge_index, dtype=torch.long)
    # transform edge_indexes for GNN
    node_features = _get_node_features(G)
    return Data(x=node_features, edge_index=edge_index.t().contiguous())
