#!/usr/bin/env bash

sudo apt-get update
sudo apt install python3-pip;
         pip install numpy==1.23.4;
         pip install torch==1.13.0;
         pip install networkx==3.0;
         pip install torch_geometric==2.3.0;
         pip install matplotlib==3.6.2;
         pip install more-itertools==8.10.0;
         pip install treelib==1.6.1;

 echo "Everything is ready!";


