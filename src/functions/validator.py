import networkx as nx
from torch_geometric.utils import to_networkx
from topology import Topology
from brute_force import calculate_equilibrium
from networkx_to_data import to_data
import math


def _add_attributes(G, x):
    """
    Adds color and stubborn attributes to the graph
    :param G: networkx.Graph()
    :param x: list of tensors - list(torch.Tensor)
    :return: returns number of empty, red and blue nodes - int, int, int

    """
    empty = 0
    red = 0
    blue = 0
    attrs = dict()

    for node in G.nodes:
        tensor = x[node]
        if tensor[5] == 1:
            if tensor[3] == 1:
                attrs[node] = {'color': 'blue', 'stubborn': True}
            else:
                attrs[node] = {'color': 'red', 'stubborn': True}
        else:
            if tensor[3] == 1:
                attrs[node] = {'color': 'blue', 'stubborn': False}
                blue += 1
            elif tensor[3] == -1:
                attrs[node] = {'color': 'red', 'stubborn': False}
                red += 1
            else:
                attrs[node] = {'color': 'white', 'stubborn': False}
                empty += 1
    nx.set_node_attributes(G, attrs)
    return empty, blue, red


def _find_msw(equilibriums):
    """
    Finds msw and returns topolgy with on MSW and with MSW
    :param equilibriums: list of topologies - list(Topology())
    :return: list of social welfares - list(int)
    """
    max_sw = []
    for e in equilibriums:
        max_sw.append(e.sw())
    msw_index = max_sw.index(max(max_sw))
    top_index = 0
    for i in range(len(max_sw)):
        if not math.isclose(max_sw[i], max_sw[msw_index]):
            top_index = i
    return max_sw[msw_index], equilibriums[msw_index], equilibriums[top_index]


def _check_results(is_max, label, msw_top, top):
    """
    Checks whether our classification was correct
    :param is_max: is assignment at MSW - boolean
    :param label: our model's prediction - int
    :param msw_top: topology with MSWE - Topology()
    :param top: topology with non MSWE - Topology()
    :return: if our classification was correct and corrected topology if needed - boolean, Topology()
    """
    if is_max == label:
        print(f'Prediction was successful, our model predicted: {label}')
        return True, None
    print(f'Prediction was NOT successful, our model falsely predicted: {label}')
    if label == 0:
        return False, to_data(msw_top.graph)
    return False, to_data(top.graph)


def validator(data, label):
    """
    Validates whether or not classification was correct
    :param data: torch_geometric.data.Data or networkx.Graph()
    :param label: Boolean - specifying label (0, 1)
    :return: Boolean
    """
    G = to_networkx(data, to_undirected=True)
    empty, blue, red = _add_attributes(G, data.x)
    topology = Topology(empty, blue, red, graph=G)
    current_sw = topology.sw()
    msw, msw_top, top = _find_msw(calculate_equilibrium(topology))
    is_max = False
    if math.isclose(current_sw, msw):
        is_max = True
    return _check_results(is_max, label, msw_top, top)
